import numpy as np
import tensorflow as tf
import pandas as pd
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.logging.set_verbosity(tf.logging.ERROR)

dataset = pd.read_table('sponge.data.txt', sep=',', header=None)
dataset = dataset.replace('?', np.nan)
dataset = dataset.fillna(dataset.mode()[0])
dataframe = dataset.reindex(np.random.permutation(dataset.index))
dataframe = pd.get_dummies(dataframe)
points = dataframe.values.astype(float)

def input_fn():
  return tf.data.Dataset.from_tensors(tf.convert_to_tensor(points, dtype=tf.float32)).repeat(1)

def nima():
  return tf.data.Dataset.from_tensors(tf.convert_to_tensor(pointchi, dtype=tf.float32)).repeat(1)

num_clusters = 10
distance_metric = tf.contrib.factorization.KMeansClustering.SQUARED_EUCLIDEAN_DISTANCE
kmeans = tf.contrib.factorization.KMeansClustering(num_clusters=num_clusters, use_mini_batch=False, distance_metric= distance_metric)

num_iterations = 10
previous_centers = None
for _ in range(num_iterations):
  kmeans.train(input_fn)
  cluster_centers = kmeans.cluster_centers()
  previous_centers = cluster_centers

cluster_indices = list(kmeans.predict_cluster_index(input_fn))
for i, point in enumerate(points):
  cluster_index = cluster_indices[i]
  center = cluster_centers[cluster_index]
  print (i,'th point is in cluster ', cluster_index)