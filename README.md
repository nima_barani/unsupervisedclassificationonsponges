# Unsupervised Clustering by K-Means on Sponge Dataset

This project aims to cluster a dataset consisted of marine sponges by using the K-means algorithm. It exports the clustered sponges to a text-file.

### Prerequisites

> [TensorFlow](https://www.tensorflow.org/install)
